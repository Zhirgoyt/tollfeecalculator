// These would be in a separate files, but since that requires 
// loading with webpack I will include it here for the sake of this project.
// Ex: import { Vehicle } from './_models/vehicle.interface';
interface VehicleInterface {
  type: string;
  tollFree: boolean;
}
class Vehicle implements VehicleInterface {
  constructor(public type: string, public tollFree: boolean) { type = type; tollFree = tollFree; }
}

class TollCalculator {
  tollFreeDates: Array<String> = [
    '2013-01-01',
    '2013-03-28',
    '2013-03-29',
    '2013-05-01',
    '2013-05-08',
    '2013-05-09',
    '2013-06-05',
    '2013-06-06',
    '2013-06-21',
    '2013-07-01',
    '2013-11-01',
    '2013-12-24',
    '2013-12-25',
    '2013-12-26',
    '2013-12-31'
  ]
  vehicles: Array<Vehicle> = [
    {type: 'car', tollFree: false},
    {type: 'motorbike', tollFree: true},
    {type: 'emergency', tollFree: true},
    {type: 'diplomat', tollFree: true},
    {type: 'foreign', tollFree: true},
    {type: 'military', tollFree: true} 
  ]

	constructor() {}

  /**
  * For finding vehicle object from string
  *
  * @param string - vehicle type
  * @return Vehicle - the vehicle object matching the type string
  */
  findVehicleType(vehicleType: string) {
    vehicleType = vehicleType.toLowerCase();
    let vehicle;
    this.vehicles.forEach(v => {
      if (v.type === vehicleType) {
        vehicle = v;
      }
    });
    return vehicle;
  }
  /**
  * For adding vehicle to array
  *
  * @param string - type of vehicle
  * @param boolean - wether the vehicle is free of toll
  */
  addVehicleType(vehicleType: string, tollFree: boolean) {
    let v: Vehicle = new Vehicle(vehicleType, tollFree);
    this.vehicles.push(v);
  }

  /**
  * Calculates the total toll fee for one day.
  * Checks that dates are in the same day.
  * Checks so that the passes are not within 1 hour (same cost), if next fee is not higher.
  * 
  * @param vehicle - the vehicle object
  * @param dates - date and time for all the passes on one day
  * @return - the total toll fee for that day
  */
	getTollFee(vehicle: Vehicle, dates: Array<Date>) {
    let latestTollTime = dates[0];
    let lastFee = this.getDateFee(latestTollTime);
    let totalFee = lastFee;
    if (!vehicle.tollFree) {
      for (let d in dates) {
       let date = dates[d];
       if (date.getDate() !== dates[0].getDate()) {
          let errorMessage = 'Passes are not confined to one day.';
          this.errorHandler(errorMessage);
          return { 'success': false, 'message': errorMessage }
        }
        if (!this.isTollFreeDate(date)) {
          let nextFee = this.getDateFee(date);
          let minuteDiff = (date.getTime()-latestTollTime.getTime())/1000/60;
          if (minuteDiff > 60 || nextFee > lastFee) {
            totalFee += nextFee;
          }
          lastFee = nextFee;
        }
        latestTollTime = date;
      }
    }
    return (totalFee > 60) ? 60 : totalFee;
	}
  isTollFreeDate(feeDate: Date) {
    if (feeDate.getDay() == 0 || feeDate.getDay() == 6) return true;
    let feeDateStr = feeDate.toISOString().toString().substring(0,10);
    if (this.tollFreeDates.indexOf(feeDateStr) >= 0) {
      return true;
    }
    return false;
  }
  /**
  * For adding date where toll is free
  *
  * @param date - toll free date to add
  */
  addTollFreeDate(tollFreeDate: Date) {
    this.tollFreeDates.push(tollFreeDate.toString().substring(0,10));
  }
  getDateFee(feeDate: Date) {
    let fee = this.checkFee(feeDate.getHours(), feeDate.getMinutes());
    return fee;
  }
  // @TODO: Would ideally be in a seperate service
  checkFee(h: number, m: number) {
    /***
    Fee is:
    0 - before 6 and after 18:30
    8 - between 6-6:30, 8:30-15, 18-18:30
    13 - between 6:30-7, 8-8:30, 15-15:30, 17-18
    18 - between 7-8, 15-16, 16-17
    ***/
    if ((h == 6 && m <= 29) || (h == 18 && m <= 29) || (h == 8 && m >= 30) || (h >= 9 && h <= 14)) {
      return  8;
    } else if ((h == 6 && m >= 30) || (h == 8 && m <= 29) || (h == 15 && m <= 29) || h == 17 ) {
      return 13;
    } else if (h == 7 || h == 15 || h == 16) {
      return 18;
    }
    return 0;
  }
  errorHandler(error: string) {
    // Do more error handling
    // Send to log
    console.log('Error: ' + error);
  }
};


/*** Tests, would not be part of "real" application ***/
let car: Vehicle = new Vehicle('car', false);
let toll = new TollCalculator();
document.body.innerHTML = "<h1>Total fee: " + toll.getTollFee(car, [new Date('2017-03-20 09:00:21'), new Date('2017-03-20 09:30:21'), new Date('2017-03-20 09:40:21'), new Date()]) + "</h1>";