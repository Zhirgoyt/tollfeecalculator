## Synopsis

A modified version of the TollFeeCalculator.
Has functions for:
- Returning total toll fee for a vehicle for a day. Returns an error if the datestamps are not in the same day.
- Finding vehicle object from type string.
- Added toll free days.
- Adding vehicle types.

## Installation

Installing typescript:
```npm install -g typescript```

Compiling:
```tsc helloworld.ts```

## Tests

To test, uncomment the lines on the bottom of the .ts-file and compile.
Open the tollCalculator.html file in a browser.